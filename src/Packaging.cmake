##### Includes #################################################################
if(NOT DEFINED ${PROJECT_NAME_L} OR NOT DEFINED ${PROJECT_VERSION})
	include(Enotork/ProjectHelper)
endif()

##### Variables ################################################################
set(CPACK_SOURCE_IGNORE_FILES
	"/.svn/" "/CVS/" "/.git/" ".gitignore$" "/.hg/" "/.hgignore$"
	"~$" ".bak$" ".old$" ".directory$"
	".swp$" ".gmo$" ".log$" ".cache$"
	"/build/" "/Build/"
	".kdev"
)
### Generators
set(CPACK_GENERATOR "TBZ2")
set(CPACK_SOURCE_GENERATOR "TBZ2")
### Output file names
set(CPACK_PACKAGE_FILE_NAME "${PROJECT_NAME_L}-${PROJECT_VERSION}")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${PROJECT_NAME_L}-${PROJECT_VERSION}")
### Versioning
set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})

##### Building #################################################################

### Build pack
include(CPack)

### Adding targets
add_custom_target("Pack-Binary" make package)
add_custom_target("Pack-Source" make package_source)
