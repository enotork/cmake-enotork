################################################################################
### hex_to_dec
# Convert hex string to decimal integer value.
#
# Args:
#	HEX		: Input string with hexadecimale number.
#	DEC		: Output variable to return decimal value.
function(hex_to_dec HEX DEC)
	### Clean HEX string
	string(TOUPPER "${HEX}" HEX)
	string(REGEX REPLACE "^0X" "" HEX ${HEX})

	### Loop for every nibble
	set(RES 0)
	string(LENGTH "${HEX}" LEN)
	while(LEN GREATER 0)
		### Shift nibbles left
		math(EXPR RES "${RES} * 16")
		string(SUBSTRING "${HEX}" 0 1 NIBBLE)
		string(SUBSTRING "${HEX}" 1 -1 HEX)
		### Convert ABCDEF
		if(NIBBLE STREQUAL "A")
			math(EXPR RES "${RES} + 10")
		elseif(NIBBLE STREQUAL "B")
			math(EXPR RES "${RES} + 11")
		elseif(NIBBLE STREQUAL "C")
			math(EXPR RES "${RES} + 12")
		elseif(NIBBLE STREQUAL "D")
			math(EXPR RES "${RES} + 13")
		elseif(NIBBLE STREQUAL "E")
			math(EXPR RES "${RES} + 14")
		elseif(NIBBLE STREQUAL "F")
			math(EXPR RES "${RES} + 15")
		else()
			math(EXPR RES "${RES} + ${NIBBLE}")
		endif()
		### New length from substring
		string(LENGTH "${HEX}" LEN)
	endwhile()
	set(${DEC} ${RES} PARENT_SCOPE)
endfunction()



################################################################################
### dec_to_hex
# Convert decimal integer value to hex string.
#
# Args:
#	DEC		: Input variable with decimal value.
#	HEX		: Output string with hexadecimale number.
function(dec_to_hex DEC HEX)
	set(RES "")
	### Loop until decimal
	while(DEC GREATER 0)
		### Nibble is the reminder of the division by 16
		math(EXPR NIBBLE "${DEC} % 16")
		### New decimal is the division by 16
		math(EXPR DEC "${DEC} / 16")
		### Convert ABCDEF
		if(NIBBLE EQUAL 10)
			set(RES "A${RES}")
		elseif(NIBBLE EQUAL 11)
			set(RES "B${RES}")
		elseif(NIBBLE EQUAL 12)
			set(RES "C${RES}")
		elseif(NIBBLE EQUAL 13)
			set(RES "D${RES}")
		elseif(NIBBLE EQUAL 14)
			set(RES "E${RES}")
		elseif(NIBBLE EQUAL 15)
			set(RES "F${RES}")
		else()
			set(RES "${NIBBLE}${RES}")
		endif()
	endwhile()
	set(${HEX} "0x${RES}" PARENT_SCOPE)
endfunction()
