### Include ARM generic flags
include(Toolchain/arm-none-eabi)

### Add specific build flags
set(CMAKE_C_FLAGS		"${CMAKE_C_FLAGS} -mthumb -mcpu=cortex-m3")
set(CMAKE_CXX_FLAGS		"${CMAKE_CXX_FLAGS} -mthumb -mcpu=cortex-m3")
set(CMAKE_ASM_FLAGS		"${CMAKE_ASM_FLAGS} -mthumb -mcpu=cortex-m3")
