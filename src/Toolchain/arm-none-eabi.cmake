### Cross-compilation variables
set(CMAKE_SYSTEM_NAME "Generic")

### Compilers executable
set(CMAKE_C_COMPILER "/usr/bin/arm-none-eabi-gcc")

### Search paths for external libraries/tools
set(CMAKE_SYSROOT "/usr/arm-none-eabi")
set(CMAKE_PREFIX_PATH "${CMAKE_SYSROOT}/usr")
set(CMAKE_MODULE_PATH "${CMAKE_SYSROOT}/usr/share/cmake/Modules")
set(CMAKE_FIND_ROOT_PATH "${CMAKE_SYSROOT}")
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

### Build flags
set(CMAKE_C_FLAGS								"-mabi=aapcs -fomit-frame-pointer -fno-common -ffunction-sections -fdata-sections -fno-builtin" CACHE INTERNAL "c compiler flags")
set(CMAKE_CXX_FLAGS								"${CMAKE_C_FLAGS}" CACHE INTERNAL "cxx compiler flags")
set(CMAKE_ASM_FLAGS								"-x assembler-with-cpp" CACHE INTERNAL "asm compiler flags")
set(CMAKE_EXE_LINKER_FLAGS						"-Wl,--gc-sections" CACHE INTERNAL "executable linker flags")
set(CMAKE_MODULE_LINKER_FLAGS					"-mabi=aapcs" CACHE INTERNAL "module linker flags")
set(CMAKE_SHARED_LINKER_FLAGS					"-mabi=aapcs" CACHE INTERNAL "shared linker flags")

### Debug build flags
set(CMAKE_C_FLAGS_DEBUG							"-O0 -g" CACHE INTERNAL "c compiler flags debug")
set(CMAKE_CXX_FLAGS_DEBUG						"-O0 -g" CACHE INTERNAL "cxx compiler flags debug")
set(CMAKE_ASM_FLAGS_DEBUG						"-g" CACHE INTERNAL "asm compiler flags debug")

### Release build flags
set(CMAKE_C_FLAGS_RELEASE						"-Os -DNDEBUG" CACHE INTERNAL "c compiler flags release")
set(CMAKE_CXX_FLAGS_RELEASE						"-Os -DNDEBUG" CACHE INTERNAL "cxx compiler flags release")
set(CMAKE_ASM_FLAGS_RELEASE						"" CACHE INTERNAL "asm compiler flags release")

### RelWithDebInfo build flags
set(CMAKE_C_FLAGS_RELWITHDEBINFO				"-O0 -g -DNDEBUG" CACHE INTERNAL "c compiler flags relwithdebinfo")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO				"-O0 -g -DNDEBUG" CACHE INTERNAL "cxx compiler flags relwithdebinfo")
set(CMAKE_ASM_FLAGS_RELWITHDEBINFO				"-g " CACHE INTERNAL "asm compiler flags relwithdebinfo")

### MinSizeRel build flags
set(CMAKE_C_FLAGS_MINSIZEREL					"-Os -DNDEBUG" CACHE INTERNAL "c compiler flags minsizerel")
set(CMAKE_CXX_FLAGS_MINSIZEREL					"-Os -DNDEBUG" CACHE INTERNAL "cxx compiler flags minsizerel")
set(CMAKE_ASM_FLAGS_MINSIZEREL					"" CACHE INTERNAL "asm compiler flags minsizerel")
